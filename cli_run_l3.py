import datetime
import logging
import pickle
import random
import time

import click

from edt.instance import get_instance
from edt.local_search import local_search
from edt.ppc import launch_mzn, lns

logging.basicConfig(
    filename="output_tests_l3_pypy/execution",
    level=logging.INFO,
    format="%(asctime)s %(message)s",
)


@click.command()
@click.option("--method", help="method used (ls, lns or ppc)", type=str)
@click.option("--randseed", help="random seed", type=int)
@click.option(
    "--light", help="1 student sectionning before, 0 no sectionning", type=bool
)
@click.option(
    "--rand_init",
    help="1 random initialisation for local search," " 0 tabou initialisation",
    type=bool,
)
@click.option(
    "--recuit",
    default=False,
    help="1 recuit for local search," " 0 tabou initialisation",
    type=bool,
)
def main(method, randseed, light, rand_init, recuit):
    random.seed(randseed)
    file = "ressources/L3_instances/week10-room5-dist1.xml"
    print(datetime.datetime.now().strftime("%H:%M:%S"))
    ins = get_instance(file, light)
    scores = None
    start_time = time.time()
    if method == "ls":
        scores = local_search(ins, rand_init=rand_init, recuit=recuit)
    elif method == "lns":
        scores = lns(ins, light=light)
    elif method == "ppc":
        launch_mzn(ins=ins, solve="all", light=light)
    t = time.time() - start_time
    logging.info(
        f"{method} {randseed} {light} {rand_init} {recuit} {t} " f"{ins.problem.name}"
    )
    h = t // 3600
    s = t - (h * 3600)
    m = s // 60
    s = s - (m * 60)
    print(f"Execution time : {int(h):02}:{int(m):02}:{int(s):02}")

    with open(
        f"output_tests_l3_pypy/{method}_{randseed}_{light}_"
        f"{rand_init}_{recuit}_"
        f"{ins.problem.name}.pck",
        "wb",
    ) as f:
        pickle.dump(scores, f)


if __name__ == "__main__":
    main()
