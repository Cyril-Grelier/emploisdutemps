import datetime

reparation_heure = 0
reparation_temps_salle = 0
reparation_echange_cours = 1

if reparation_heure:
    # Changer l'heure d'un cours
    from edt.instance import get_instance

    file = "ressources/L3_instances/week10-room5-dist1.xml"

    print(file)
    print(datetime.datetime.now().strftime("%H:%M:%S"))
    ins = get_instance(file)
    print("réparation après suppression de l'heure d'un cours")
    ins.read_solution("ressources/solutions/solution_week10-room5-dist1_to_modify.xml")

    ins.delete_time_for_class("Images de Synthèse_TP-1")
    from edt.web import launch_on_web

    launch_on_web(ins)

if reparation_temps_salle:
    # retirer une possibilite de temps pour une salle
    from edt.instance import get_instance

    file = "ressources/L3_instances/week10-room5-dist1.xml"

    print(file)
    print(datetime.datetime.now().strftime("%H:%M:%S"))

    print("réparation après suppression de d'une heure de dipo d'une salle")
    ins = get_instance(file)

    ins.read_solution("ressources/solutions/solution_week10-room5-dist1_to_modify.xml")

    from edt.instance_objects import Time

    t1 = Time(days="00001", weeks="1100000000", start=3, length=1)
    t2 = Time(days="00001", weeks="1100000000", start=4, length=1)
    ins.delete_times_for_room("H003", [t1, t2])
    from edt.web import launch_on_web

    launch_on_web(ins)

if reparation_echange_cours:
    # echanger un cours entre profs
    from edt.instance import get_instance

    file = "ressources/L3_instances/week10-room5-dist1.xml"

    print(file)
    print(datetime.datetime.now().strftime("%H:%M:%S"))
    ins = get_instance(file)
    print("échange de cours entre deux profs")
    ins.read_solution("ressources/solutions/solution_week10-room5-dist1_to_modify.xml")

    id_c1 = "Programmation Logique_TP-A"
    id_c2 = "Développement Web_TP-1-B"
    ins.swap_teachers(id_c1, id_c2)

    from edt.web import launch_on_web

    launch_on_web(ins)
