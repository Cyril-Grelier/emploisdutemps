"""
Gestion des contraintes de répartition
"""
from abc import ABC, abstractmethod
from typing import List, Tuple

from edt.instance_objects import Class, Room, Time


def dont_overlap_and_enough_time(
    time1: Time, room1: Tuple[Room, int], time2: Time, room2: Tuple[Room, int]
) -> bool:
    """Retourne Vrai si la contrainte SameAttendees est respectée

    Args:
        time1 (Time): créneau séance 1
        room1 (Tuple[Room, int]): salle séance 1
        time2 (Time): créneau séance 2
        room2 (Tuple[Room, int]): salle séance 2

    Returns:
        bool: Vrai si SameAttendees ok
    """
    return not (
        (
            not (time1.days_bool & time2.days_bool)
            or not (time1.weeks_bool & time2.weeks_bool)
        )
        or (time1.end + room1[0].travel[room2[0].int_id - 1][1] <= time2.start)
        or (time2.end + room2[0].travel[room1[0].int_id - 1][1] <= time1.start)
    )


def can_merge(b, ba, s) -> bool:
    """Vrai si les blocs peuvent être rassemblés"""
    if ba[0] == b[0]:
        b[1] = max(b[1], ba[1])
        return True
    if b[1] + s >= ba[0] >= b[0]:
        b[1] = max(b[1], ba[1])
        return True
    return False


def merge_blocks(bs, s):
    """Rassemblement des blocs"""
    blocks = []
    for ba in bs:
        added = False
        for b in blocks:
            if can_merge(b, ba, s):
                added = True
        if not added:
            blocks.append(ba)
    return blocks


class Distribution(ABC):
    """Représentation des contraintes de répartition"""

    correspondance_noms = {
        "SameStart": "same_start",
        "SameTime": "same_time",
        "SameDays": "same_days",
        "SameWeeks": "same_weeks",
        "SameRoom": "same_room",
        "DifferentTime": "different_time",
        "DifferentDays": "different_days",
        "DifferentWeeks": "different_weeks",
        "DifferentRoom": "different_room",
        "Overlap": "overlap",
        "SameAttendees": "same_attendees",
        "Precedence": "precedence",
        "WorkDay": "work_day",
        "MinGap": "min_gap",
        "NotOverlap": "not_overlap",
        "MaxDays": "max_days",
        "MaxDayLoad": "max_day_load",
        "MaxBreaks": "max_breaks",
        "MaxBlock": "max_block",
    }

    def __init__(self, type_, required, penalty, classes: List[Class]):
        self.type = type_
        self.required = required
        self.penalty = penalty
        self.classes = classes
        self.nom_mzn = Distribution.correspondance_noms[type_]
        self.problem = None

    def __repr__(self):
        s = f"Distribution : {self.type}"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    @abstractmethod
    def check_constraint(self) -> bool:
        """Calcul des contraintes

        Returns:
            bool: Vrai si aucun problème
        """

    @abstractmethod
    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """


class SameStart(Distribution):
    """
    Short Description:
    All classes in this constraint must start at the same time of day.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must start at the same time slot,
        regardless of their days of week or weeks.
        This means that Ci.start = Cj.start for any
        two classes Ci and Cj from the constraint;
        Ci.start is the assigned start time slot of a class Ci.
        :return: bool
        """
        start_times = set(c.time.start for c in self.classes)
        if len(start_times) > 1:
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        start_times = set(c.time.start for c in self.classes if c.time is not None)
        if len(start_times) > 1:
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True


class SameTime(Distribution):
    """
    Short Description:
    All classes in this constraint must be offered
    within the same times of day as those required
    by the longest class in the set, i.e.,
    the times of day they meet must be completely
    overlapped by the longest class in the constraint.
    """

    def check_constraint(self) -> bool:
        """
         Given classes must be taught at the same time of day,
         regardless of their days of week or weeks.
         For the classes of the same length, this is the same
         constraint as SameStart (classes must start at the same time slot).
         For the classes of different lengths,
         the shorter class can start after the longer class but must end
         before or at the same time as the longer class. This means that
         (Ci.start ≤ Cj.start ∧ Cj.end ≤ Ci.end)
         ∨
         (Cj.start ≤ Ci.start ∧ Ci.end ≤ Cj.end)
         for any two classes Ci and Cj from the constraint;
         Ci.end = Ci.start + Ci.length
         is the assigned end time slot of a class Ci.

        :return: bool
        """
        lgt_c = None
        max_length = -1
        for c in self.classes:
            if max_length < c.time.length:
                lgt_c = c
                max_length = c.time.length
        if any(
            (c.time.start < lgt_c.time.start) or (c.time.end > lgt_c.time.end)
            for c in self.classes
        ):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        lgt_c = None
        max_length = -1
        for c in self.classes:
            if c.time is not None:
                if max_length < c.time.length:
                    lgt_c = c
                    max_length = c.time.length
        if any(
            (c.time.start < lgt_c.time.start) or (c.time.end > lgt_c.time.end)
            for c in self.classes
            if c.time is not None
        ):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True


class DifferentTime(Distribution):
    """
    Short Description:
    The times of day during which all classes
    in this constraint meet can not overlap.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must be taught during different times of day,
        regardless of their days of week or weeks.
        This means that no two classes of this constraint can overlap
        at a time of the day. This means that
        (Ci.end ≤ Cj.start) ∨ (Cj.end ≤ Ci.start)
        for any two classes Ci and Cj from the constraint.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                ci = self.classes[i].time
                cj = self.classes[j].time
                if not ((ci.end <= cj.start) or (cj.end <= ci.start)):
                    if self.required:
                        for c in self.classes:
                            c.increment_hard()
                    else:
                        for c in self.classes:
                            c.increment_soft(self.penalty)
                    return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            ci = self.classes[i].time
            if ci is not None:
                for j in range(i + 1, len(self.classes)):
                    cj = self.classes[j].time
                    if cj is not None:
                        if not ((ci.end <= cj.start) or (cj.end <= ci.start)):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True


class SameDays(Distribution):
    """
    Short Description:
    All classes in this constraint must be offered on the same days
    of the week or, if a class meets fewer days, it must meet on a subset
    of the days used by the class with the largest number of days.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must be taught on the same days,
        regardless of their start time slots and weeks.
        In case of classes of different days of the week,
        a class with fewer meetings must meet on a subset
        of the days used by the class with more meetings.
        For example, if the class with the most meetings meets
        on Monday–Tuesday–Wednesday, all others classes in the
        constraint can only be taught on Monday, Wednesday,
        and/or Friday. This means that
        ((Ci.days or Cj.days) = Ci.days) ∨ ((Ci.days or Cj.days) = Cj.days)
        for any two classes Ci and Cj from the constraint;
        Ci.days are the assigned days of the week of a class Ci,
        doing binary "or" between the bit strings.

        :return: bool
        """
        positions = [c.time.days for c in self.classes]
        if len(set(positions[0]).intersection(*positions)) != min(
            [len(p) for p in positions]
        ):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        positions = [c.time.days for c in self.classes if c.time is not None]
        if positions:
            if len(set(positions[0]).intersection(*positions)) != min(
                [len(p) for p in positions]
            ):
                if self.required:
                    for c in self.classes:
                        c.increment_hard()
                else:
                    for c in self.classes:
                        c.increment_soft(self.penalty)
                return False
        return True


class DifferentDays(Distribution):
    """
    Short Description:
    All classes in this constraint can not meet on
    any of the same days of the week.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must be taught on different days of the week,
        regardless of their start time slots and weeks. This means that
        (Ci.days and Cj.days) = 0
        for any two classes Ci and Cj from the constraint;
        doing binary "and" between the bit strings representing the
        assigned days.

        :return: bool
        """
        positions = [p for c in self.classes for p in c.time.days]
        if len(positions) != len(set(positions)):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        positions = [p for c in self.classes if c.time is not None for p in c.time.days]
        if positions:
            if len(positions) != len(set(positions)):
                if self.required:
                    for c in self.classes:
                        c.increment_hard()
                else:
                    for c in self.classes:
                        c.increment_soft(self.penalty)
                return False
        return True


class SameWeeks(Distribution):
    """
    Short Description:
    All classes in this constraint must be offered during
    the same weeks of the term or, if a class meets fewer weeks,
    it must meet during a subset of the weeks used by the class
    meeting for the largest number of weeks.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must be taught in the same weeks,
        regardless of their time slots or days of the week.
        In case of classes of different weeks, a class with
        fewer weeks must meet on a subset of the weeks used
        by the class with more weeks.
        This means that
        (Ci.weeks or Cj.weeks) = Ci.weeks) ∨
        (Ci.weeks or Cj.weeks) = Cj.weeks)
        for any two classes Ci and Cj from the constraint;
        doing binary "or" between the bit strings representing
        the assigned weeks.

        :return: bool
        """
        positions = [c.time.weeks for c in self.classes]
        if len(set(positions[0]).intersection(*positions)) != min(
            [len(p) for p in positions]
        ):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        positions = [c.time.weeks for c in self.classes if c.time is not None]
        if positions:
            if len(set(positions[0]).intersection(*positions)) != min(
                [len(p) for p in positions]
            ):
                if self.required:
                    for c in self.classes:
                        c.increment_hard()
                else:
                    for c in self.classes:
                        c.increment_soft(self.penalty)
                return False
        return True


class DifferentWeeks(Distribution):
    """
    Short Description:
    The weeks of the term during which any classes
    in this constraint meet can not overlap.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must be taught on different weeks,
        regardless of their time slots or days of the week.
        This means that
        (Ci.weeks and Cj.weeks) = 0
        for any two classes Ci and Cj from the constraint;
        doing binary "and" between the bit strings representing
        the assigned weeks.

        :return: bool
        """
        positions = [p for c in self.classes for p in c.time.weeks]
        if len(positions) != len(set(positions)):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        positions = [
            p for c in self.classes if c.time is not None for p in c.time.weeks
        ]
        if len(positions) != len(set(positions)):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True


class Overlap(Distribution):
    """
    Short Description:
    Any two classes in this constraint must have some overlap in time.
    """

    def check_constraint(self) -> bool:
        """
         Given classes overlap in time. Two classes overlap in time when
         they overlap in time of day, days of the week, as well as weeks.
         This means that
         (Cj.start < Ci.end) ∧ (Ci.start < Cj.end) ∧
         ((Ci.days and Cj.days) ≠ 0) ∧ ((Ci.weeks and Cj.weeks) ≠ 0)
         for any two classes Ci and Cj from the constraint, doing binary
         "and" between days and weeks of Ci and Cj.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                c1 = self.classes[i].time
                c2 = self.classes[j].time
                if not (
                    (c2.start < c1.end)
                    and (c1.start < c2.end)
                    and (
                        (c1.days_bool & c2.days_bool)
                        and (c1.weeks_bool & c2.weeks_bool)
                    )
                ):
                    if self.required:
                        for c in self.classes:
                            c.increment_hard()
                    else:
                        for c in self.classes:
                            c.increment_soft(self.penalty)
                    return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            c1 = self.classes[i].time
            if c1 is not None:
                for j in range(i + 1, len(self.classes)):
                    c2 = self.classes[j].time
                    if c2 is not None:
                        if not (
                            (c2.start < c1.end)
                            and (c1.start < c2.end)
                            and (
                                (c1.days_bool & c2.days_bool)
                                and (c1.weeks_bool & c2.weeks_bool)
                            )
                        ):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True


class NotOverlap(Distribution):
    """
    Short Description:
    No two classes in this constraint can overlap in time.
    """

    def check_constraint(self) -> bool:
        """
        Given classes do not overlap in time.
        Two classes do not overlap in time when
        they do not overlap in time of day, or in
        days of the week, or in weeks. This means that
        (Ci.end ≤ Cj.start) ∨ (Cj.end ≤ Ci.start) ∨
        ((Ci.days and Cj.days) = 0) ∨ ((Ci.weeks and Cj.weeks) = 0)
        for any two classes Ci and Cj from the constraint,
        doing binary "and" between days and weeks of Ci and Cj.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                c1 = self.classes[i].time
                c2 = self.classes[j].time
                if not (
                    (c1.end <= c2.start)
                    or (c2.end <= c1.start)
                    or (
                        not c1.days_bool & c2.days_bool
                        or (not c1.weeks_bool & c2.weeks_bool)
                    )
                ):
                    if self.required:
                        for c in self.classes:
                            c.increment_hard()
                    else:
                        for c in self.classes:
                            c.increment_soft(self.penalty)
                    return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            c1 = self.classes[i].time
            if c1 is not None:
                for j in range(i + 1, len(self.classes)):
                    c2 = self.classes[j].time
                    if c2 is not None:
                        if not (
                            (c1.end <= c2.start)
                            or (c2.end <= c1.start)
                            or (
                                not c1.days_bool & c2.days_bool
                                or not c1.weeks_bool & c2.weeks_bool
                            )
                        ):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True


class SameRoom(Distribution):
    """
    Short Description:
    All classes in this constraint must be assigned to the same room.
    """

    def check_constraint(self) -> bool:
        """
        Given classes should be placed in the same room.
        This means that (Ci.room = Cj.room) for any two
        classes Ci and Cj from the constraint;
        Ci.room is the assigned room of Ci.

        :return: bool
        """
        if len(set(c.room[0].int_id for c in self.classes)) > 1:
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        if len(set(c.room[0].int_id for c in self.classes if c.room is not None)) > 1:
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True


class DifferentRoom(Distribution):
    """
    Short Description:
    All classes in this constraint must be assigned to different rooms.
    """

    def check_constraint(self) -> bool:
        """
        Given classes should be placed in different rooms.
        This means that (Ci.room ≠ Cj.room) for any two classes
        Ci and Cj from the constraint.

        :return: bool
        """
        room_ids = [c.room[0].int_id for c in self.classes]
        if len(set(room_ids)) != len(room_ids):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        room_ids = [c.room[0].int_id for c in self.classes if c.room is not None]
        if len(set(room_ids)) != len(room_ids):
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True


class SameAttendees(Distribution):
    """
    Short Description:
    All classes in this constraint must meet at times
    and locations such that someone who must attend, e.g.,
    an instructor, is reasonably able to attend all classes.
    This means that no two classes can overlap in time or follow
    one another without sufficient time to travel between the
    two class locations.
    """

    def check_constraint(self) -> bool:
        """
        Given classes cannot overlap in time, and if they are
        placed on overlapping days of week and weeks, they must
        be placed close enough so that the attendees can travel
        between the two classes. This means that
        (Ci.end + Ci.room.travel[Cj.room] ≤ Cj.start) ∨
        (Cj.end + Cj.room.travel[Ci.room] ≤ Ci.start) ∨
        ((Ci.days and Cj.days) = 0) ∨ ((Ci.weeks and Cj.weeks) = 0)
        for any two classes Ci and Cj from the constraint;
        Ci.room.travel[Cj.room] is the travel time between
        the assigned rooms of Ci and Cj.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                ci = self.classes[i]
                cj = self.classes[j]
                if dont_overlap_and_enough_time(ci.time, ci.room, cj.time, cj.room):
                    if self.required:
                        for c in self.classes:
                            c.increment_hard()
                    else:
                        for c in self.classes:
                            c.increment_soft(self.penalty)
                    return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            ci = self.classes[i]
            if ci.time is not None and ci.room is not None:
                for j in range(i + 1, len(self.classes)):
                    cj = self.classes[j]
                    if cj.time is not None and cj.room is not None:
                        if dont_overlap_and_enough_time(
                            ci.time, ci.room, cj.time, cj.room
                        ):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True


class Precedence(Distribution):
    """
    Short Description:
    This constraint establishes an ordered listing which
    requires that the first meeting of each of the listed
    classes occurs in its entirety prior to the first meeting
    of all subsequently listed classes.
    """

    def check_constraint(self) -> bool:
        """
        Given classes must be one after the other in the order
        provided in the constraint definition. For classes that
        have multiple meetings in a week or that are on different
        weeks, the constraint only cares about the first meeting
        of the class.
        That is,
        the first class starts on an earlier week or
        they start on the same week and the first class starts
        on an earlier day of the week or
        they start on the same week and day of the week and
        the first class is earlier in the day.

        This means that
        (first(Ci.weeks) < first(Cj.weeks)) ∨
        [ (first(Ci.weeks) = first(Cj.weeks)) ∧
        [ (first(Ci .days) < first(Cj .days)) ∨
        ((first(Ci.days) = first(Cj.days)) ∧ (Ci.end ≤ Cj.start))
        ]
        ]
        for any two classes Ci and Cj from the constraint
        where i < j and first(x) is the index of the first non-zero bit
        in the binary string x.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                ci = self.classes[i].time
                cj = self.classes[j].time
                if not (
                    (ci.weeks[0] < cj.weeks[0])
                    or (
                        (ci.weeks[0] == cj.weeks[0])
                        and (
                            (ci.days[0] < cj.days[0])
                            or (ci.days[0] == cj.days[0])
                            and (ci.end <= cj.start)
                        )
                    )
                ):
                    if self.required:
                        for c in self.classes:
                            c.increment_hard()
                    else:
                        for c in self.classes:
                            c.increment_soft(self.penalty)
                    return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            ci = self.classes[i].time
            if ci is not None:
                for j in range(i + 1, len(self.classes)):
                    cj = self.classes[j].time
                    if cj is not None:
                        if not (
                            (ci.weeks[0] < cj.weeks[0])
                            or (
                                (ci.weeks[0] == cj.weeks[0])
                                and (
                                    (ci.days[0] < cj.days[0])
                                    or (ci.days[0] == cj.days[0])
                                    and (ci.end <= cj.start)
                                )
                            )
                        ):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True


class WorkDay(Distribution):
    """
    Short Description:
    This constraint prevents or penalizes the occurrence
    of class pairs among the listed classes where the time
    between the start of one class and the end of another
    which occur on the same day is greater than S time slots.
    """

    def __init__(self, type_, required, penalty, classes, s):
        super().__init__(type_, required, penalty, classes)
        self.s = s * 5 / 60

    def __repr__(self):
        s = f"Distribution : {self.type} ({self.s})"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    def check_constraint(self) -> bool:
        """
        There should not be more than S time slots between the start
        of the first class and the end of the last class on any given day.
        This means that classes that are placed on the overlapping days
        and weeks that have more than S time slots between the start
        of the earlier class and the end of the later class are
        violating the constraint.
        That is
        ((Ci.days and Cj.days) = 0) ∨ ((Ci.weeks and Cj.weeks) = 0)
        ∨ (max(Ci.end,Cj.end)−min(Ci.start,Cj.start) ≤ S)
        for any two classes Ci and Cj from the constraint.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                ci = self.classes[i].time
                cj = self.classes[j].time
                if ci.days_bool & cj.days_bool:
                    if ci.weeks_bool & cj.weeks_bool:
                        if not (
                            (max(ci.end, cj.end) - min(ci.start, cj.start)) <= self.s
                        ):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            ci = self.classes[i].time
            if ci is not None:
                for j in range(i + 1, len(self.classes)):
                    cj = self.classes[j].time
                    if cj is not None:
                        if ci.days_bool & cj.days_bool:
                            if ci.weeks_bool & cj.weeks_bool:
                                if not (
                                    (max(ci.end, cj.end) - min(ci.start, cj.start))
                                    <= self.s
                                ):
                                    if self.required:
                                        for c in self.classes:
                                            c.increment_hard()
                                    else:
                                        for c in self.classes:
                                            c.increment_soft(self.penalty)
                                    return False
        return True


class MinGap(Distribution):
    """
    Short Description:
    The times assigned to any two classes in this constraint
    that are placed on the same day must allow for at least G slots
    between the end of the earlier class and the start of the later class.
    """

    def __init__(self, type_, required, penalty, classes, g):
        super().__init__(type_, required, penalty, classes)
        self.g = g * 5 / 60

    def __repr__(self):
        s = f"Distribution : {self.type} ({self.g})"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    def check_constraint(self) -> bool:
        """
        Any two classes that are taught on the same day
        (they are placed on overlapping days and weeks)
        must be at least G slots apart.
        This means that there must be at least G slots between
        the end of the earlier class and the start of the later class.
        That is
        ((Ci.days and Cj.days) = 0) ∨ ((Ci.weeks and Cj.weeks) = 0)
        ∨ (Ci.end + G ≤ Cj.start) ∨ (Cj.end + G ≤ Ci.start)
        for any two classes Ci and Cj from the constraint.

        :return: bool
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                ci = self.classes[i].time
                cj = self.classes[j].time
                if ci.days_bool & cj.days_bool:
                    if ci.weeks_bool & cj.weeks_bool:
                        if not (
                            (ci.end + self.g <= cj.start)
                            or (cj.end + self.g <= ci.start)
                        ):
                            if self.required:
                                for c in self.classes:
                                    c.increment_hard()
                            else:
                                for c in self.classes:
                                    c.increment_soft(self.penalty)
                            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        for i in range(len(self.classes)):
            ci = self.classes[i].time
            if ci is not None:
                for j in range(i + 1, len(self.classes)):
                    cj = self.classes[j].time
                    if cj is not None:
                        if ci.days_bool & cj.days_bool:
                            if ci.weeks_bool & cj.weeks_bool:
                                if not (
                                    (ci.end + self.g <= cj.start)
                                    or (cj.end + self.g <= ci.start)
                                ):
                                    if self.required:
                                        for c in self.classes:
                                            c.increment_hard()
                                    else:
                                        for c in self.classes:
                                            c.increment_soft(self.penalty)
                                    return False
        return True


class MaxDays(Distribution):
    """
    Short Description:
    The classes in this constraint should not be assigned
    to more than D different weekdays, regardless of whether
    this occurs during the same week of a term.
    """

    def __init__(self, type_, required, penalty, classes, d):
        super().__init__(type_, required, penalty, classes)
        self.d = d

    def __repr__(self):
        s = f"Distribution : {self.type} ({self.d})"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    def check_constraint(self) -> bool:
        """
        Given classes cannot spread over more than D days of the week,
        regardless whether they are in the same week of semester or not.
        This means that the total number of days of the week that have at
        least one class of this distribution constraint C1, …, Cn is
        not greater than D,
        countNonzeroBits(C1.days or C2.days or ⋅ ⋅ ⋅ Cn.days) ≤ D
        where countNonzeroBits(x) returns the number of non-zero bits
        in the bit string x. When the constraint is soft,
        the penalty is multiplied by the number of days that exceed
        the given constant D.

        :return: bool
        """
        days = [t for c in self.classes for t in c.time.days]
        if len(days) > self.d:
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        days = [t for c in self.classes if c.time is not None for t in c.time.days]
        if len(days) > self.d:
            if self.required:
                for c in self.classes:
                    c.increment_hard()
            else:
                for c in self.classes:
                    c.increment_soft(self.penalty)
            return False
        return True


class MaxDayLoad(Distribution):
    """
    Short Description:
    This constraint limits the total amount of time assigned
    to the set of classes listed in the constraint to no more
    than S time slots per day over the entire term.
    """

    def __init__(self, type_, required, penalty, classes, s):
        super().__init__(type_, required, penalty, classes)
        self.s = s

    def __repr__(self):
        if self.type == "Precedence" and self.required:
            print("la")
        s = f"Distribution : {self.type} ({self.s})"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    def check_constraint(self) -> bool:
        """
        Given classes must be spread over the days of the week
        (and weeks) in a way that there is no more than a given
        number of S time slots on every day.
        This means that for each week w ∈ {0, 1, …, nrWeeks − 1} of
        the semester and each day of the week d ∈ {0, 1, …, nrDays − 1},
        the total number of slots assigned to classes C that overlap with
        the selected day d and week w is not more than S,
        DayLoad(d,w) ≤ S
        DayLoad(d,w) =
        ∑i {Ci.length | (Ci.days and 2d) ≠ 0 ∧ (Ci.weeks and 2w) ≠ 0)}

        where 2^d is a bit string with the only non-zero bit on position
        d and 2w is a bit string with the only non-zero bit on position w.
        When the constraint is soft (it is not required and there is a
        penalty), its penalty is multiplied by the number of slots that exceed
        the given constant S over all days of the semester and divided by the
        number of weeks of the semester (using integer division).
        Importantly the integer division is computed at the very end. That is
        (penalty × ∑w,dmax(DayLoad(d,w) − S, 0)) / nrWeeks

        :return: bool
        """
        # days = [t for c in self.classes for t in c.time.days]
        # if len(days) > self.s:
        #     if self.required:
        #         list(map(lambda c: c.increment_hard(), self.classes))
        #     else:
        #         list(map(lambda c: c.increment_soft(self.penalty),
        #         self.classes))
        all_lengths = []
        for d in [1 << di for di in range(self.problem.nrDays)]:
            d_lengths = []
            for w in [1 << wi for wi in range(self.problem.nrWeeks)]:
                w_lengths = []
                for c in self.classes:
                    ct = c.time
                    if (ct.days_bool & d) and (ct.weeks_bool & w):
                        w_lengths.append(ct.length)
                d_lengths.append(sum(w_lengths))
            all_lengths.append(d_lengths)
        if self.required:
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    if all_lengths[d][w] > self.s:
                        for c in self.classes:
                            c.increment_hard()
                            return False
        else:
            sum_p = 0
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    sum_p += max(all_lengths[d][w] - self.s, 0)
            if sum_p != 0:
                p = (self.penalty * sum_p) / self.problem.nrWeeks
                for c in self.classes:
                    c.increment_soft(p)
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        all_lengths = []
        for d in [1 << di for di in range(self.problem.nrDays)]:
            d_lengths = []
            for w in [1 << wi for wi in range(self.problem.nrWeeks)]:
                w_lengths = []
                for c in self.classes:
                    if c.time is not None:
                        ct = c.time
                        if (ct.days_bool & d) and (ct.weeks_bool & w):
                            w_lengths.append(ct.length)
                d_lengths.append(sum(w_lengths))
            all_lengths.append(d_lengths)
        if self.required:
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    if all_lengths[d][w] > self.s:
                        for c in self.classes:
                            c.increment_hard()
                            return False
        else:
            sum_p = 0
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    sum_p += max(all_lengths[d][w] - self.s, 0)
            if sum_p != 0:
                p = (self.penalty * sum_p) / self.problem.nrWeeks
                for c in self.classes:
                    c.increment_soft(p)
        return True


class MaxBreaks(Distribution):
    """
    Short Description:
    This constraint limits the number of breaks between classes during
    a day which exceed S time slots to no more than R per day.
    """

    def __init__(self, type_, required, penalty, classes, r, s):
        super().__init__(type_, required, penalty, classes)
        self.r = r
        self.s = s

    def __repr__(self):
        s = f"Distribution : {self.type} ({self.r}, {self.s})"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    def check_constraint(self) -> bool:
        """
        This constraint limits the number of breaks during a day between
        a given set of classes (not more than R breaks during a day).
        For each day of week and week, there is a break between classes
        if there is more than S empty time slots in between.

        Two consecutive classes are considered to be in the same block
        if the gap between them is not more than S time slots.
        This means that for each week w ∈ {0, 1, …, nrWeeks − 1}
        of the semester and each day of the week d ∈ {0, 1, …, nrDays − 1},
        the number of blocks is not greater than R + 1,
        | MergeBlocks{(C.start, C.end) | (C.days and 2d) ≠ 0 ∧
        (C.weeks and 2w) ≠ 0})| ≤ R + 1

        where 2d is a bit string with the only non-zero bit on position
        d and 2w is a bit string with the only non-zero bit on position w.
        The MergeBlocks function recursively merges to the block B any two
        blocks Ba and Bb that are identified by their start and end slots
        that overlap or are not more than S slots apart, until there are no
        more blocks that could be merged.
        (Ba.end + S ≥ Bb.start) ∧ (Bb.end + S ≥ Ba.start) ⇒
        (B.start = min(Ba.start, Bb.start)) ∧ (B.end = max(Ba.end, Bb.end))
        When the constraint is soft, the penalty is multiplied by the total
        number of additional breaks computed over each day of the week and
        week of the semester and divided by the number of weeks of the
        semester at the end
        (using integer division, just like for the MaxDayLoad constraint).

        :return: bool


        Cette contrainte limite le nombre de pause entre séances pendant une
        journée qui dépasse S timeslots à pas plus de R par jour

        Deux classes conscutives sont dans le même block si l'écart entre elles
        n'est pas de plus de S timeslots.
        Donc pour chaque semaines w de [0,...,nrWeeks -1]

        """
        results = []
        for d in [1 << di for di in range(self.problem.nrDays)]:
            resultsd = []
            for w in [1 << wi for wi in range(self.problem.nrWeeks)]:
                blocks = []
                for c in self.classes:
                    ct = c.time
                    if (ct.days_bool & d) and (ct.weeks_bool & w):
                        blocks.append([ct.start, ct.end])
                blocks.sort(key=lambda x: (x[0], x[1] - x[0]))
                resultsd.append(len(merge_blocks(blocks, self.s)))
            results.append(resultsd)
        if self.required:
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    if results[d][w] > self.r + 1:
                        for c in self.classes:
                            c.increment_hard()
                            return False
        else:
            sum_p = 0
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    sum_p += max(results[d][w] - (self.r + 1), 0)
            if sum_p != 0:
                p = (self.penalty * sum_p) / self.problem.nrWeeks
                for c in self.classes:
                    c.increment_soft(p)
                return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        results = []
        for d in [1 << di for di in range(self.problem.nrDays)]:
            resultsd = []
            for w in [1 << wi for wi in range(self.problem.nrWeeks)]:
                blocks = []
                for c in self.classes:
                    ct = c.time
                    if ct is not None:
                        if (ct.days_bool & d) and (ct.weeks_bool & w):
                            blocks.append([ct.start, ct.end])
                blocks.sort(key=lambda x: (x[0], x[1] - x[0]))
                resultsd.append(len(merge_blocks(blocks, self.s)))
            results.append(resultsd)
        if self.required:
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    if results[d][w] > self.r + 1:
                        for c in self.classes:
                            c.increment_hard()
                            return False
        else:
            sum_p = 0
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    sum_p += max(results[d][w] - (self.r + 1), 0)
            if sum_p != 0:
                p = (self.penalty * sum_p) / self.problem.nrWeeks
                for c in self.classes:
                    c.increment_soft(p)
                return False
        return True


class MaxBlock(Distribution):
    """
    Short Description:
    This constraint limits the amount of time (measured as M slots)
    that a set of classes may be consecutively scheduled to which are
    each separated by no more than S slots.
    """

    def __init__(self, type_, required, penalty, classes, m, s):
        super().__init__(type_, required, penalty, classes)
        self.m = m
        self.s = s

    def __repr__(self):
        s = f"Distribution : {self.type} ({self.m}, {self.s})"
        s += " (required) : " if self.required else f" ({self.penalty}) : "
        s += f"{[c.id for c in self.classes]}"
        return s

    def check_constraint(self) -> bool:
        """
        This constraint limits the length of a block of two or more consecutive
        classes during a day (not more than M slots in a block).
        For each day of week and week, two consecutive classes are considered
        to be in the same block if the gap between them is not more than S
        time slots. For each block, the number of time slots from the start
        of the first class in a block till the end of the last class in a
        block must not be more than M time slots. This means that for each
        week w ∈ {0, 1, …, nrWeeks − 1} of the semester and each day of the
        week d ∈ {0, 1, …, nrDays − 1}, the maximal length of a block does
        not exceed M slots
        max { B.end − B.start | B ∈ MergeBlocks{(C.start, C.end)
        | (C.days and 2d) ≠ 0 ∧ (C.weeks and 2w) ≠ 0})}) ≤ M

        Please note that only blocks of two or more classes are considered.
        In other words, if there is a class longer than M slots, it does not
        break the constraint by itself, but it cannot form a larger block
        with another class.

        When the constraint is soft, the penalty is multiplied by the total
        number of blocks that are over the M time slots, computed over each
        day of the week and week of the semester and divided by the number
        of weeks of the semester at the end (using integer division, just
        like for the MaxDayLoad constraint).

        :return: bool
        """
        results = []
        for d in [1 << di for di in range(self.problem.nrDays)]:
            resultsd = []
            for w in [1 << wi for wi in range(self.problem.nrWeeks)]:
                blocks = []
                for c in self.classes:
                    ct = c.time
                    if (ct.days_bool & d) and (ct.weeks_bool & w):
                        blocks.append([ct.start, ct.end])
                blocks.sort(key=lambda x: (x[0], x[1] - x[0]))
                if len(blocks) <= 1:
                    resultsd.append(0)
                else:
                    resultsd.append(
                        max([a[1] - a[0] for a in merge_blocks(blocks, self.s)])
                    )
            results.append(resultsd)
        if self.required:
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    if results[d][w] > self.m:
                        for c in self.classes:
                            c.increment_hard()
                            return False
        else:
            sum_p = 0
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    sum_p += max(results[d][w] - self.m, 0)
            if sum_p != 0:
                p = (self.penalty * sum_p) / self.problem.nrWeeks
                for c in self.classes:
                    c.increment_soft(p)
                return False
        return True

    def check_partial_constraint(self) -> bool:
        """Calcul des contraintes en prenant en compte que les séances placées

        Returns:
            bool: Vrai si aucun problème
        """
        results = []
        for d in [1 << di for di in range(self.problem.nrDays)]:
            resultsd = []
            for w in [1 << wi for wi in range(self.problem.nrWeeks)]:
                blocks = []
                for c in self.classes:
                    ct = c.time
                    if ct is not None:
                        if (ct.days_bool & d) and (ct.weeks_bool & w):
                            blocks.append([ct.start, ct.end])
                blocks.sort(key=lambda x: (x[0], x[1] - x[0]))
                if len(blocks) <= 1:
                    resultsd.append(0)
                else:
                    resultsd.append(
                        max([a[1] - a[0] for a in merge_blocks(blocks, self.s)])
                    )
            results.append(resultsd)
        if self.required:
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    if results[d][w] > self.m:
                        for c in self.classes:
                            c.increment_hard()
                            return False
        else:
            sum_p = 0
            for d in range(self.problem.nrDays):
                for w in range(self.problem.nrWeeks):
                    sum_p += max(results[d][w] - self.m, 0)
            if sum_p != 0:
                p = (self.penalty * sum_p) / self.problem.nrWeeks
                for c in self.classes:
                    c.increment_soft(p)
                return False
        return True


def get_object_from_str(type_name: str) -> Distribution:
    """Retourne la contrainte demandée

    Args:
        type_name (str): nom de la contrainte

    Returns:
        Distribution: la contrainte
    """
    return globals()[type_name]


class PseudoSameAttendees:
    """Représentation des groupes d'étudiants"""

    def __init__(self, penalty: int, classes: List[Class]):
        self.classes = classes
        self.penalty = penalty

    def check_constraint(self, problem_same_attends: List[Tuple[Class, Class]]) -> None:
        """
        Calcul des contraintes

        Args:
            problem_same_attends (List[Tuple[Class, Class]]): liste pour relever les
            erreurs
        """
        for i in range(len(self.classes)):
            for j in range(i + 1, len(self.classes)):
                ci = self.classes[i]
                cj = self.classes[j]
                if dont_overlap_and_enough_time(ci.time, ci.room, cj.time, cj.room):
                    ci.student_clashes += self.penalty
                    cj.student_clashes += self.penalty
                    problem_same_attends.append((ci, cj))

    def check_partial_constraint(
        self, problem_same_attends: List[Tuple[Class, Class]]
    ) -> None:
        """Calcul des contraintes en prenant en compte que les séances placées

        Args:
            problem_same_attends (List[Tuple[Class, Class]]): liste pour relever les
            erreurs
        """
        for i in range(len(self.classes)):
            ci = self.classes[i]
            if ci.time is not None and ci.room is not None:
                for j in range(i + 1, len(self.classes)):
                    cj = self.classes[j]
                    if cj.time is not None and cj.room is not None:
                        if dont_overlap_and_enough_time(
                            ci.time, ci.room, cj.time, cj.room
                        ):
                            ci.student_clashes += self.penalty
                            cj.student_clashes += self.penalty
                            problem_same_attends.append((ci, cj))
