"""
Gestion de la recherche locale
"""
import platform
import random
import time
from datetime import datetime, timedelta
from typing import List, Optional, Generator, Tuple

from edt.instance import Instance, Score
from edt.instance_objects import Class

if platform.python_implementation() == "CPython":
    import matplotlib.pyplot as plt

VERBOSE = False


def condition_fin(
    scores: List[Score], rang: int, turn: int, max_time: timedelta, start_time: datetime
) -> bool:
    """Check condition de fin pour l'initialisation tabou

    Args:
        scores (List[Score]): Liste des score précédants
        rang (int): rang de l'exécution
        turn (int): itération de l'exécution
        max_time (timedelta): temps max d'exécution
        start_time (datetime): temps de départ d'exécution

    Returns:
        bool: True si la recherche doit continuer
    """
    if turn == -1:
        return True
    if start_time + max_time < datetime.now():
        return False
    if rang <= 2:
        return turn + 1 < rang
    return len(set(scores[-100:])) > 3 and turn + 1 < rang


def randomly_initialize(ins: Instance) -> None:
    """Initialisation aléatoire de l'instance

    Args:
        ins (Instance): instance à initialiser
    """
    for classe in ins.all_classes:
        classe.time = random.choices(
            population=classe.times, weights=[t.penalty * -100 for t in classe.times]
        )[0]
        classe.room = random.choices(
            population=classe.classrooms,
            weights=[r[1] * -100 for r in classe.classrooms],
        )[0]


def tabou_initialize(
    ins: Instance, recuit: bool = False, max_time=None, start_time=None
) -> List[Score]:
    """Initialisation tabou

    Args:
        ins (Instance): instance à initialiser
        recuit (bool, optional): recuit ou non. Defaults to False.
        max_time ([type], optional): temps maximum d'exécution. Defaults to None.
        start_time ([type], optional): temps de départ d'exécution. Defaults to None.

    Returns:
        [type]: [description]
    """
    scores = []
    past_selected = []
    temps = time.time()
    v_max = (
        sum([1 if i < 20 else 2 + int(i / 100) for i in range(len(ins.all_classes))])
        + 1
    )
    tolerate = 1
    tot_turn = 0
    for i in range(len(ins.all_classes)):
        score = ins.get_partial_score()
        turn = -1
        while condition_fin(
            scores,
            1 if i < 20 else 2 + int(i / 100),
            turn,
            max_time=max_time,
            start_time=start_time,
        ):
            turn += 1
            tot_turn += 1
            if turn == 0:
                selected = i
            else:
                indexes = list(range(i + 1))
                for j in past_selected[-10:]:
                    indexes.remove(j)
                total_clashes = [
                    ins.all_classes[ind].total_soft_clashes()
                    + ins.all_classes[ind].total_hard_clashes() * 1000000
                    for ind in indexes
                ]
                selected = random.choices(population=indexes, weights=total_clashes)[0]
            selected_class = ins.all_classes[selected]
            if VERBOSE:
                elapsed = time.time() - temps
                print(
                    f"turn TS {turn:3} ({i}/{len(ins.all_classes)})",
                    f"score : {score} selected : {selected:3} " f"time : {elapsed:.3f}",
                )
            past_selected.append(selected)
            new_scores = []
            for r_i, room_c in enumerate(selected_class.classrooms):
                for t_i, time_c in enumerate(selected_class.times):
                    selected_class.room = room_c
                    selected_class.time = time_c
                    new_score = ins.get_partial_score_class(selected_class)
                    new_scores.append(((r_i, t_i), new_score))
            new_scores.sort(key=lambda x: x[1].hard)
            min_score = min(new_scores, key=lambda x: x[1].hard * 1000000 + x[1].soft)
            if recuit:
                tolerate = 1 + ((v_max - tot_turn) * 4 / v_max)
            next_val = random.choice(
                [
                    s
                    for s in new_scores
                    if s[1].hard * 1000000 + s[1].soft
                    <= (min_score[1].hard * 1000000 + min_score[1].soft) * tolerate
                ]
            )
            # next_val = min(new_scores,
            #                key=lambda x: (x[1].hard * 1000000, x[1].soft))
            selected_class.room = selected_class.classrooms[next_val[0][0]]
            selected_class.time = selected_class.times[next_val[0][1]]
            scores.append(ins.get_partial_score)
    # if VERBOSE:
    #     save_plot_score(scores, context='initialisation tabou')
    return scores


def select_class(ins: Instance, past_selected: List[int]) -> Class:
    """Sélectionne et retourne la prochaine classe à modifier

    Args:
        ins (Instance): instance à gérer
        past_selected (List[int]): liste tabou

    Returns:
        Class: la classe à modifier
    """
    indexes = list(range(len(ins.all_classes)))
    for j in past_selected[-min(int(len(ins.all_classes) / 4), 100) :]:
        indexes.remove(j)
    total_clashes = [
        ins.all_classes[i].total_soft_clashes()
        + ins.all_classes[i].total_hard_clashes() * 1000000
        for i in indexes
    ]
    selected = random.choices(population=indexes, weights=total_clashes)[0]
    past_selected.append(selected)
    return ins.all_classes[selected]


def get_neighborhood(selected: Class) -> Generator[Tuple[int, int], None, None]:
    """Génère le voisinage de la classe choisie

    Args:
        selected (Class): classe choisie

    Yields:
        Generator[Tuple[int, int], None, None]: indice de la salle et du créneau
    """
    for r_i in range(len(selected.classrooms)):
        for t_i in range(len(selected.times)):
            yield r_i, t_i


def ls_continue(scores: List[Score], max_time: timedelta, start_time: datetime) -> bool:
    """Condition de fin pour la recherche locale

    Args:
        scores (List[Score]): liste des scores précédents
        max_time (timedelta): temps d'exécution max
        start_time (datetime): temps de départ d'exécution

    Returns:
        bool: True si la recherche doit continuer
    """
    if start_time + max_time < datetime.now():
        return False
    if len(scores) <= 20:
        return True
    return len(set(scores[-100:])) > 3


def local_search(
    ins: Instance,
    rand_init: bool = False,
    max_time: timedelta = timedelta(hours=2),
    recuit: bool = False,
) -> List[Score]:
    """Recherche locale

    Args:
        ins (Instance): instance à traiter
        rand_init (bool, optional): False si initialisation tabou. Defaults to False.
        max_time (timedelta, optional): Temps maximum. Defaults to timedelta(hours=2).
        recuit (bool, optional): True si recuit simulé. Defaults to False.

    Returns:
        List[Score]: liste des scores de la recherche
    """
    start_time = datetime.now()
    scores_tabou = []
    if rand_init:
        randomly_initialize(ins)
    else:
        scores_tabou = tabou_initialize(
            ins, recuit=recuit, max_time=max_time, start_time=start_time
        )
    scores = []
    score = ins.get_score()
    temps = time.time()
    v_max = len(ins.all_classes) * 3
    tolerate = 1
    past_selected = []
    turn = -1
    while ls_continue(scores, max_time, start_time):
        scores.append(score)
        turn += 1
        selected = select_class(ins, past_selected)
        if VERBOSE:
            elapsed = time.time() - temps
            nb_neighbors = len(selected.classrooms) * len(selected.times)
            print(
                f"turn LS {turn:3} ",
                f"score : {score} "
                f"time : {elapsed:.3f} nb neighbors : {nb_neighbors}",
            )
        new_scores = []
        for r_i, room_c in enumerate(selected.classrooms):
            for t_i, time_c in enumerate(selected.times):
                selected.room = room_c
                selected.time = time_c
                new_score = ins.get_partial_score_class(selected)
                new_scores.append(((r_i, t_i), new_score))
        new_scores.sort(key=lambda x: x[1].hard)
        min_score = min(new_scores, key=lambda x: x[1].hard * 1000000 + x[1].soft)
        min_value = min_score[1].hard * 1000000 + min_score[1].soft
        if recuit:
            tolerate = 1 + ((v_max - turn) * 4 / v_max)
        pop = [
            s
            for s in new_scores
            if s[1].hard * 1000000 + s[1].soft <= min_value * max(tolerate, 1)
        ]
        next_val = random.choice(pop)
        selected.room = selected.classrooms[next_val[0][0]]
        selected.time = selected.times[next_val[0][1]]
        score = ins.get_score()
    scores.append(score)
    if len(scores_tabou) != 0:
        scores = scores_tabou + [None] + scores
    if VERBOSE:
        save_plot_score(scores, context="local search")
    return scores


def save_plot_score(scores: List[Optional[Score]], context: str = "") -> None:
    """affiche le graphique d'exécution

    Args:
        scores (List[Optional[Score]]): scores obtenus lors de l'exécution
        context (str, optional): nom du graphe. Defaults to "".
    """
    if platform.python_implementation() == "CPython":
        if None in scores:
            t_s = scores[0 : scores.index(None)]
            l_s = scores[scores.index(None) + 1 : :]
            save_plot_score(t_s, "initialisation tabou")
            save_plot_score(l_s, "recherche locale")
        else:
            size = list(range(len(scores)))
            _, axes = plt.subplots()
            axes.plot(size, [s.hard for s in scores], label="Hard", color="r")
            axes.plot(size, [s.soft for s in scores], label="Soft", color="#FFA500")
            axes.legend(loc="upper right")
            axes.set_xlabel("tours")
            axes.set_ylabel("nb contraintes")
            axes.set_title(context)

        plt.show()
