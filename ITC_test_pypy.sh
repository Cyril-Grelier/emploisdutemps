#!/bin/bash

source /home/user/Documents/venv/bin/activate

nb_run=5

files=(
  "agh-ggis-spr17.xml"
  "agh-ggos-spr17.xml")

files=(
  "iku-spr18.xml"
  "muni-fsps-spr17c.xml")

files=(
  "muni-pdf-spr16.xml"
"muni-fspsx-fal17.xml"
"tg-fal17.xml" "tg-spr18.xml" "lums-spr18.xml"
  "lums-fal17.xml" "bet-sum18.xml"
"lums-sum17.xml"
  "wbg-fal10.xml"
 "iku-fal17.xml")


files=(
  "muni-fi-spr17.xml"
  "yach-fal17.xml"
  "pu-cs-fal07.xml"
  "muni-pdfx-fal17.xml")

for file in "${files[@]}"; do
  pypy3 cli_run_ITC_pypy.py --file $file --randseed $nb_run
done
