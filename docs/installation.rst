.. highlight:: shell

============
Installation
============


Pour récupérer le code :
.. code-block:: console

    $ git clone git@gitlab.com:Cyril_Grl/emploisdutemps.git

Installer les différents outils : 

Pour installer minizinc et xmlstarlet:

.. code-block:: console

    $ sudo snap install minizinc --classic
    $ sudo apt install xmlstarlet

Pour créer l'environnement (si vous changez le nom de l'environnement pensez à changer le nom dans le Makefile ligne 4 VENVNAME) :

.. code-block:: console

    $ python3.8 -m venv venv-etd

Pour installer les différents outils aux dernières versions : 

.. code-block:: console

    $ pip install --upgrade -r requirements.txt




https://github.com/mozilla/geckodriver/releases pour installer le driver pour lancer sur le site web avec Firefox (voir pour installation du site dans emploisdutemps/theleme/TOUPDATE_README.md )

.. code-block:: console

    $ export PATH=$PATH:/chemin-vers-fichier-geckodriver




.. _Github repo: https://gitlab.com/Cyril_Grl/emploisdutemps