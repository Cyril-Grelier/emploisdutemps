edt package
===========

Submodules
----------

edt.distributions module
------------------------

.. automodule:: edt.distributions
   :members:
   :undoc-members:
   :show-inheritance:

edt.instance module
-------------------

.. automodule:: edt.instance
   :members:
   :undoc-members:
   :show-inheritance:

edt.instance\_objects module
----------------------------

.. automodule:: edt.instance_objects
   :members:
   :undoc-members:
   :show-inheritance:

edt.local\_search module
------------------------

.. automodule:: edt.local_search
   :members:
   :undoc-members:
   :show-inheritance:

edt.parse\_xml\_objects module
------------------------------

.. automodule:: edt.parse_xml_objects
   :members:
   :undoc-members:
   :show-inheritance:

edt.ppc module
--------------

.. automodule:: edt.ppc
   :members:
   :undoc-members:
   :show-inheritance:

edt.student\_sectionning module
-------------------------------

.. automodule:: edt.student_sectionning
   :members:
   :undoc-members:
   :show-inheritance:

edt.tools module
----------------

.. automodule:: edt.tools
   :members:
   :undoc-members:
   :show-inheritance:

edt.web module
--------------

.. automodule:: edt.web
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: edt
   :members:
   :undoc-members:
   :show-inheritance:
