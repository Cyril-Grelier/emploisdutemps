
.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   modules
   authors

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
