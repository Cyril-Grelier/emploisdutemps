#/bin/sh
# script d'assemblage des fichiers problèmes (un par UE + fichier global) et du fichier solution.

#1.1 Assemblage de chaque fichier problème UE
cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-an-comment.xml \
    ./src/edt-problem-1819-l3info-s2-an-cou.xml \
    ./src/edt-problem-1819-l3info-s2-an-dis.xml \
    ./src/edt-problem-1819-l3info-s2-an-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-an.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-bd-comment.xml \
    ./src/edt-problem-1819-l3info-s2-bd-cou.xml \
    ./src/edt-problem-1819-l3info-s2-bd-dis.xml \
    ./src/edt-problem-1819-l3info-s2-bd-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-bd.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-dw-comment.xml \
    ./src/edt-problem-1819-l3info-s2-dw-cou.xml \
    ./src/edt-problem-1819-l3info-s2-dw-dis.xml \
    ./src/edt-problem-1819-l3info-s2-dw-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-dw.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-is-comment.xml \
    ./src/edt-problem-1819-l3info-s2-is-cou.xml \
    ./src/edt-problem-1819-l3info-s2-is-dis.xml \
    ./src/edt-problem-1819-l3info-s2-is-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-is.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-pa-comment.xml \
    ./src/edt-problem-1819-l3info-s2-pa-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pa-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pa-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-pa.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-pf-comment.xml \
    ./src/edt-problem-1819-l3info-s2-pf-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pf-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pf-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-pf.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-pl-comment.xml \
    ./src/edt-problem-1819-l3info-s2-pl-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pl-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pl-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-pl.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-qt-comment.xml \
    ./src/edt-problem-1819-l3info-s2-qt-cou.xml \
    ./src/edt-problem-1819-l3info-s2-qt-dis.xml \
    ./src/edt-problem-1819-l3info-s2-qt-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-qt.xml;

cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-si-comment.xml \
    ./src/edt-problem-1819-l3info-s2-si-cou.xml \
    ./src/edt-problem-1819-l3info-s2-si-dis.xml \
    ./src/edt-problem-1819-l3info-s2-si-stu.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-0.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-0.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml delete-0.xml \
    > ./src/edt-problem-1819-l3info-s2-si.xml;

#1.2 Assemblage du fichier problème
## with comments
cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-comment.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-an-comment.xml \
    ./src/edt-problem-1819-l3info-s2-bd-comment.xml \
    ./src/edt-problem-1819-l3info-s2-dw-comment.xml \
    ./src/edt-problem-1819-l3info-s2-is-comment.xml \
    ./src/edt-problem-1819-l3info-s2-pa-comment.xml \
    ./src/edt-problem-1819-l3info-s2-pf-comment.xml \
    ./src/edt-problem-1819-l3info-s2-pl-comment.xml \
    ./src/edt-problem-1819-l3info-s2-qt-comment.xml \
    ./src/edt-problem-1819-l3info-s2-si-comment.xml \
    ./src/edt-problem-1819-l3info-s2-an-cou.xml \
    ./src/edt-problem-1819-l3info-s2-bd-cou.xml \
    ./src/edt-problem-1819-l3info-s2-dw-cou.xml \
    ./src/edt-problem-1819-l3info-s2-is-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pa-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pf-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pl-cou.xml \
    ./src/edt-problem-1819-l3info-s2-qt-cou.xml \
    ./src/edt-problem-1819-l3info-s2-si-cou.xml \
    ./src/edt-problem-1819-l3info-s2-an-dis.xml \
    ./src/edt-problem-1819-l3info-s2-bd-dis.xml \
    ./src/edt-problem-1819-l3info-s2-dw-dis.xml \
    ./src/edt-problem-1819-l3info-s2-is-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pa-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pf-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pl-dis.xml \
    ./src/edt-problem-1819-l3info-s2-qt-dis.xml \
    ./src/edt-problem-1819-l3info-s2-si-dis.xml \
    ./src/edt-problem-1819-l3info-s2-all-stu.xml \
    > delete-0.xml;

## without comments
cat \
    ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    ./src/edt-problem-1819-l3info-s2-all-header.txt \
    ./src/edt-problem-1819-l3info-s2-all-opt.txt \
    ./src/edt-problem-1819-l3info-s2-all-roo.xml \
    ./src/edt-problem-1819-l3info-s2-an-cou.xml \
    ./src/edt-problem-1819-l3info-s2-bd-cou.xml \
    ./src/edt-problem-1819-l3info-s2-dw-cou.xml \
    ./src/edt-problem-1819-l3info-s2-is-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pa-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pf-cou.xml \
    ./src/edt-problem-1819-l3info-s2-pl-cou.xml \
    ./src/edt-problem-1819-l3info-s2-qt-cou.xml \
    ./src/edt-problem-1819-l3info-s2-si-cou.xml \
    ./src/edt-problem-1819-l3info-s2-an-dis.xml \
    ./src/edt-problem-1819-l3info-s2-bd-dis.xml \
    ./src/edt-problem-1819-l3info-s2-dw-dis.xml \
    ./src/edt-problem-1819-l3info-s2-is-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pa-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pf-dis.xml \
    ./src/edt-problem-1819-l3info-s2-pl-dis.xml \
    ./src/edt-problem-1819-l3info-s2-qt-dis.xml \
    ./src/edt-problem-1819-l3info-s2-si-dis.xml \
    ./src/edt-problem-1819-l3info-s2-all-stu.xml \
    > delete-0.xml;

cat delete-0.xml \
    ./src/edt-problem-1819-l3info-s2-all-footer.txt \
    > delete-1.xml;
sed -i -E -e 's/^\<\?xml.*$//g' -e 's/^ \<\!DOCTYPE.*$//g' delete-1.xml;
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml \
    delete-1.xml \
    > delete-2.xml;
tr -d '[\r\n]' < delete-2.xml \
   > delete-3.xml;
sed -E -e "s/<\/courses>[^<]*<courses>//g" -e "s/<\/distributions>[^<]*<distributions>//g" -e "s/<\/students>[^<]*<students>//g" delete-3.xml \
    > ./src/edt-problem.xml;
#    > ./src/edt-problem-1819-l3info-s2.xml;

## fichier solution
tr -d '[\r\n]' < ./src/edt-problem-1819-l3info-s2-all-sol.xml \
       > ./src/edt-solution-1819-l3info-s2.xml;

rm delete-*;

#2 copie des fichiers obtenus dans ./datasets
cp ./src/edt-problem-1819-l3info-s2-an.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-bd.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-dw.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-is.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-pa.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-pf.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-pl.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-qt.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2-si.xml ./datasets/;
cp ./src/edt-problem-1819-l3info-s2.xml ./datasets/;
#cp ./src/edt-problem.xml ./datasets/edt-problem-1819-l3info-s2.xml;
cp ./src/edt-solution-1819-l3info-s2.xml ./datasets/;

#3.1
## renommage de la DTD
find ./datasets -type f -name "*.xml" -print0 | xargs -0 \
sed -i -E -e 's/UA 2020\/\/DTD Problem Format\/EN" "edt-problem.dtd"/ITC 2019\/\/DTD Problem Format\/EN" "http:\/\/www.itc2019.org\/competition-format.dtd"/g';

#3.2 transforme les rangs des créneaux en minutes : rang 1 <=> 8h <=> 97 minutes pour interface web !
xd=5;
xvd=288;
xL=1;
xvL=16;
xs1=1;
xv1=97;
xs2=2;
xv2=115;
xs3=3;
xv3=133;
xs4=4;
xv4=169;
xs5=5;
xv5=187;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/slotsPerDay=\"$xd\"/slotsPerDay=\"$xvd\"/"`;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/length=\"$xL\"/length=\"$xvL\"/g"`;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs1\"/start=\"$xv1\"/g"`;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs2\"/start=\"$xv2\"/g"`;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs3\"/start=\"$xv3\"/g"`;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs4\"/start=\"$xv4\"/g"`;
find ./datasets -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs5\"/start=\"$xv5\"/g"`;

rm ./datasets/*-E;

#4 copie des fichiers problème et solution à la racine
cp ./datasets/edt-problem-1819-l3info-s2.xml problem.xml;
cp ./datasets/edt-solution-1819-l3info-s2.xml solution.xml;

