# Interface Web et scripts de génération des fichiers XML 

## Construire l'interface web
	1. Exécuter `npm install` dans le dossier `./web/stageproject`
	2. Exécuter `ng build --prod` dans le dossier `./web/stageproject`
	3. Copier le dossier `./web/stageproject/dist` résultant dans `./web/web`
	5. Ajustez identifiant et mot de passe de connexion au SGBD MySQL dans `./web/web/api/connexpdo.inc.php`.
	6. Créer une base de données mySQL de nom `edt`.
	7. Exécuter le script `./database/edt.sql` pour créer tables et enregistrements dans la base. 
	8. Ajouter au fichier `httpd.conf` de configuration du serveur Apache le fragment suivant : 

	Alias /edt PATH-TO-ROOT-DIR/web/web
	<Directory "PATH-TO-ROOT-DIR/web/web">
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
	</Directory>

	9. Redémarrer le serveur Apache avec `sudo apachectl restart`
 
 
## Utiliser l'interface Web
	1. Accéder à l'interface à http://localhost/edt/dist/stageproject.
	2. Entrez systématiquement la date d'un lundi au format `2020-06-01`.
	3. Problème et solution sont stockées au format XML dans `./web/web/dist/app/XML. Pour visualiser la solution XML, aller dans l'un des onglets `Etudiant`, `Professeur`, `Salle`, `UE`, sélectionner un élément (étudiant, etc) et `créer edt`.
 

## Pour modifier/re-générer les fichiers XML
	1. Exécuter `./build.sh` pour re-générer les fichiers `./problem.xml` et `./solution.xml` à partir des fichiers sources situés dans `./src` (voir explications ci-dessous).
	2. Pour lier les fichiers problème et solution de l'interface web à ces deux fichiers, exécuter `rm ./web/web/dist/stageproject/app/XML/*.xml; ln -s problem.xml ./web/web/dist/stageproject/app/XML/.; ln -s solution.xml ./web/web/dist/stageproject/app/XML/.`


## Automatisation des fichiers problème et solution
	- A chaque UE correspond un code (eg. `an` pour Anglais) et différents fichiers sources dans `./src dont :
		-- `edt-problem-l3info-s2-*CODE UE*-cou.xml : le fichier des cours de l'UE
		-- `edt-problem-l3info-s2-*CODE UE*-dis.xml : le fichier des contraintes de distributions de l'UE
	- Les fichiers `./problem.xml` et `./solution.xml` sont obtenus en assemblant les *fichiers UE* et d'autres fichiers propres au problème ou à la solution dont 
		-- `edt-problem-l3info-s2-all-roo.xml : le fichier des salles
		-- `edt-problem-l3info-s2-all-stu.xml : le fichier des étudiants
		-- `edt-problem-l3info-s2-all-sol.xml : la solution
	- Toute modification sur l'un de ces fragments sera automatiquement répercutée par `./build.sh` dans le fichier problème et/ou solution.
	- Le processus d'assemblage avec `./build.sh` génère dans `./datasets` un fichier problème par UE en utilisant le fichier étudiant propre à l'UE :
			-- `edt-problem-l3info-s2-*CODE UE*-stu.xml : le fichier des étudiants de l'UE
	