#/bin/sh
# 1. copie les fichiers XML de ./jeux-avec-name dans ./jeux-avec-id
# 2. les transforme pour rendre la visualisation possible par l'interface web
# 3. copie dans le répertoire racine les deux fichiers problème et solution

# 1.
cp -r ./jeux-avec-name/*.xml ./jeux-avec-id;

# 2.
## renommage de la DTD
find ./jeux-avec-id -type f -name "*.xml" -print0 | xargs -0 \
sed -i -E -e 's/UA 2020\/\/DTD Problem Format\/EN" "edt-problem.dtd"/ITC 2019\/\/DTD Problem Format\/EN" "http:\/\/www.itc2019.org\/competition-format.dtd"/g';

## copie des attributs 'name' dans 'id' et supression des attributs 'name' et 'code' pour assurer la conformité à la DTD "competition ITC"
find ./jeux-avec-id -type f -name "*.xml" -print0 | xargs -0 \
sed -i -E -e 's/course id="1" name="Anglais"/course id="Anglais"/g' \
-e 's/course id="2" name="Bases_de_Données"/course id="Bases de Données"/g' \
-e 's/course id="3" name="Développement_Web"/course id="Développement Web"/g' \
-e 's/course id="4" name="Programmation_Fonctionnelle"/course id="Programmation Fonctionnelle"/g' \
-e 's/course id="5" name="Programmation_Logique"/course id="Programmation Logique"/g' \
-e 's/course id="6" name="Images_de_Synthèse"/course id="Images de Synthèse"/g' \
-e 's/course id="7" name="Production_Automatisée_de_Documents"/course id="Production Automatisée de Documents"/g' \
-e 's/course id="8" name="Qt_Avancé"/course id="Qt Avancé"/g' \
-e 's/course id="9" name="Systèmes_Intelligents"/course id="Systèmes Intelligents"/g' \
-e 's/course id="1"/course id="Anglais"/g' \
-e 's/course id="2"/course id="Bases de Données"/g' \
-e 's/course id="3"/course id="Développement Web"/g' \
-e 's/course id="4"/course id="Programmation Fonctionnelle"/g' \
-e 's/course id="5"/course id="Programmation Logique"/g' \
-e 's/course id="6"/course id="Images de Synthèse"/g' \
-e 's/course id="7"/course id="Production Automatisée de Documents"/g' \
-e 's/course id="8"/course id="Qt Avancé"/g' \
-e 's/course id="9"/course id="Systèmes Intelligents"/g' \
-e 's/class id="1010" name="AN-TP-A" limit="20"/class id="Anglais_TP-A" limit="20"/g' \
-e 's/class id="1011" name="AN-TP-B" limit="20"/class id="Anglais_TP-B" limit="20"/g' \
-e 's/class id="1012" name="AN-TP-C" limit="20"/class id="Anglais_TP-C" limit="20"/g' \
-e 's/class id="2000" name="BD-CM-1" limit="80"/class id="Bases de Données_CM-1" limit="80"/g' \
-e 's/class id="2010" name="BD-CM-2" limit="80"/class id="Bases de Données_CM-2" limit="80"/g' \
-e 's/class id="2020" name="BD-TD-A" limit="40"/class id="Bases de Données_TD-A" limit="40"/g' \
-e 's/class id="2021" name="BD-TD-B" limit="40"/class id="Bases de Données_TD-B" limit="40"/g' \
-e 's/class id="2030" name="BD-TP-A" limit="20"/class id="Bases de Données_TP-A" limit="20"/g' \
-e 's/class id="2031" name="BD-TP-B" limit="20"/class id="Bases de Données_TP-B" limit="20"/g' \
-e 's/class id="2032" name="BD-TP-C" limit="20"/class id="Bases de Données_TP-C" limit="20"/g' \
-e 's/class id="3000" name="DW-CM-1" limit="80"/class id="Développement Web_CM-1" limit="80"/g' \
-e 's/class id="3010" name="DW-CM-2" limit="80"/class id="Développement Web_CM-2" limit="80"/g' \
-e 's/class id="3020" name="DW-TP-1-A" limit="20"/class id="Développement Web_TP-1-A" limit="20"/g' \
-e 's/class id="3021" name="DW-TP-1-B" limit="20"/class id="Développement Web_TP-1-B" limit="20"/g' \
-e 's/class id="3022" name="DW-TP-1-C" limit="20"/class id="Développement Web_TP-1-C" limit="20"/g' \
-e 's/class id="3030" name="DW-TP-2-A" limit="20" parent="1020"/class id="Développement Web_TP-2-A" limit="20" parent="Développement Web_TP-1-A"/g' \
-e 's/class id="3031" name="DW-TP-2-B" limit="20" parent="1021"/class id="Développement Web_TP-2-B" limit="20" parent="Développement Web_TP-1-B"/g' \
-e 's/class id="3032" name="DW-TP-2-C" limit="20" parent="1022"/class id="Développement Web_TP-2-C" limit="20" parent="Développement Web_TP-1-C"/g' \
-e 's/class id="4000" name="PF-CM-1" limit="80"/class id="Programmation Fonctionnelle_CM-1" limit="80"/g' \
-e 's/class id="4010" name="PF-CM-2" limit="80"/class id="Programmation Fonctionnelle_CM-2" limit="80"/g' \
-e 's/class id="4020" name="PF-TD-A" limit="40"/class id="Programmation Fonctionnelle_TD-A" limit="40"/g' \
-e 's/class id="4021" name="PF-TD-B" limit="40"/class id="Programmation Fonctionnelle_TD-B" limit="40"/g' \
-e 's/class id="4030" name="PF-TP-A" limit="20"/class id="Programmation Fonctionnelle_TP-A" limit="20"/g' \
-e 's/class id="4031" name="PF-TP-B" limit="20"/class id="Programmation Fonctionnelle_TP-B" limit="20"/g' \
-e 's/class id="4032" name="PF-TP-C" limit="20"/class id="Programmation Fonctionnelle_TP-C" limit="20"/g' \
-e 's/class id="5000" name="PL-CM-1" limit="80"/class id="Programmation Logique_CM-1" limit="80"/g' \
-e 's/class id="5010" name="PL-CM-2" limit="80"/class id="Programmation Logique_CM-2" limit="80"/g' \
-e 's/class id="5020" name="PL-TD-A" limit="40"/class id="Programmation Logique_TD-A" limit="40"/g' \
-e 's/class id="5021" name="PL-TD-B" limit="40"/class id="Programmation Logique_TD-B" limit="40"/g' \
-e 's/class id="5030" name="PL-TP-A" limit="20"/class id="Programmation Logique_TP-A" limit="20"/g' \
-e 's/class id="5031" name="PL-TP-B" limit="20"/class id="Programmation Logique_TP-B" limit="20"/g' \
-e 's/class id="5032" name="PL-TP-C" limit="20"/class id="Programmation Logique_TP-C" limit="20"/g' \
-e 's/class id="6000" name="IS-CM-1" limit="20"/class id="Images de Synthèse_CM-1" limit="20"/g' \
-e 's/class id="6010" name="IS-CM-2" limit="20"/class id="Images de Synthèse_CM-2" limit="20"/g' \
-e 's/class id="6020" name="IS-TP-1" limit="20"/class id="Images de Synthèse_TP-1" limit="20"/g' \
-e 's/class id="6030" name="IS-TP-2" limit="20"/class id="Images de Synthèse_TP-2" limit="20"/g' \
-e 's/class id="7000" name="PA-CM-1" limit="80"/class id="Production Automatisée de Documents_CM-1" limit="80"/g' \
-e 's/class id="7010" name="PA-CM-2" limit="80"/class id="Production Automatisée de Documents_CM-2" limit="80"/g' \
-e 's/class id="7020" name="PA-TD-1" limit="40"/class id="Production Automatisée de Documents_TD-1" limit="40"/g' \
-e 's/class id="7030" name="PA-TD-2" limit="40"/class id="Production Automatisée de Documents_TD-2" limit="40"/g' \
-e 's/class id="7040" name="PA-TP-1" limit="40"/class id="Production Automatisée de Documents_TP-1" limit="40"/g' \
-e 's/class id="7050" name="PA-TP-2" limit="40"/class id="Production Automatisée de Documents_TP-2" limit="40"/g' \
-e 's/class id="8000" name="QT-CM-1" limit="80"/class id="Qt Avancé_CM-1" limit="80"/g' \
-e 's/class id="8010" name="QT-CM-2" limit="80"/class id="Qt Avancé_CM-2" limit="80"/g' \
-e 's/class id="8020" name="QT-TP-1" limit="40"/class id="Qt Avancé_TP-1" limit="40"/g' \
-e 's/class id="8030" name="QT-TP-2" limit="40"/class id="Qt Avancé_TP-2" limit="40"/g' \
-e 's/class id="9000" name="SI-CM-1" limit="80"/class id="Systèmes Intelligents_CM-1" limit="80"/g' \
-e 's/class id="9010" name="SI-CM-2" limit="80"/class id="Systèmes Intelligents_CM-2" limit="80"/g' \
-e 's/class id="9020" name="SI-TD-1" limit="40"/class id="Systèmes Intelligents_TD-1" limit="40"/g' \
-e 's/class id="9030" name="SI-TD-2" limit="40"/class id="Systèmes Intelligents_TD-2" limit="40"/g' \
-e 's/class id="9040" name="SI-TP-1" limit="40"/class id="Systèmes Intelligents_TP-1" limit="40"/g' \
-e 's/class id="9050" name="SI-TP-2" limit="40"/class id="Systèmes Intelligents_TP-2" limit="40"/g' \
-e 's/class id="1010"/class id="Anglais_TP-A"/g' \
-e 's/class id="1011"/class id="Anglais_TP-B"/g' \
-e 's/class id="1012"/class id="Anglais_TP-C"/g' \
-e 's/class id="2000"/class id="Bases de Données_CM-1"/g' \
-e 's/class id="2010"/class id="Bases de Données_CM-2"/g' \
-e 's/class id="2020"/class id="Bases de Données_TD-A"/g' \
-e 's/class id="2021"/class id="Bases de Données_TD-B"/g' \
-e 's/class id="2030"/class id="Bases de Données_TP-A"/g' \
-e 's/class id="2031"/class id="Bases de Données_TP-B"/g' \
-e 's/class id="2032"/class id="Bases de Données_TP-C"/g' \
-e 's/class id="3000"/class id="Développement Web_CM-1"/g' \
-e 's/class id="3010"/class id="Développement Web_CM-2"/g' \
-e 's/class id="3020"/class id="Développement Web_TP-1-A"/g' \
-e 's/class id="3021"/class id="Développement Web_TP-1-B"/g' \
-e 's/class id="3022"/class id="Développement Web_TP-1-C"/g' \
-e 's/class id="3030"/class id="Développement Web_TP-2-A"/g' \
-e 's/class id="3031"/class id="Développement Web_TP-2-B"/g' \
-e 's/class id="3032"/class id="Développement Web_TP-2-C"/g' \
-e 's/class id="4000"/class id="Programmation Fonctionnelle_CM-1"/g' \
-e 's/class id="4010"/class id="Programmation Fonctionnelle_CM-2"/g' \
-e 's/class id="4020"/class id="Programmation Fonctionnelle_TD-A"/g' \
-e 's/class id="4021"/class id="Programmation Fonctionnelle_TD-B"/g' \
-e 's/class id="4030"/class id="Programmation Fonctionnelle_TP-A"/g' \
-e 's/class id="4031"/class id="Programmation Fonctionnelle_TP-B"/g' \
-e 's/class id="4032"/class id="Programmation Fonctionnelle_TP-C"/g' \
-e 's/class id="5000"/class id="Programmation Logique_CM-1"/g' \
-e 's/class id="5010"/class id="Programmation Logique_CM-2"/g' \
-e 's/class id="5020"/class id="Programmation Logique_TD-A"/g' \
-e 's/class id="5021"/class id="Programmation Logique_TD-B"/g' \
-e 's/class id="5030"/class id="Programmation Logique_TP-A"/g' \
-e 's/class id="5031"/class id="Programmation Logique_TP-B"/g' \
-e 's/class id="5032"/class id="Programmation Logique_TP-C"/g' \
-e 's/class id="6000"/class id="Images de Synthèse_CM-1"/g' \
-e 's/class id="6010"/class id="Images de Synthèse_CM-2"/g' \
-e 's/class id="6020"/class id="Images de Synthèse_TP-1"/g' \
-e 's/class id="6030"/class id="Images de Synthèse_TP-2"/g' \
-e 's/class id="7000"/class id="Production Automatisée de Documents_CM-1"/g' \
-e 's/class id="7010"/class id="Production Automatisée de Documents_CM-2"/g' \
-e 's/class id="7020"/class id="Production Automatisée de Documents_TD-1"/g' \
-e 's/class id="7030"/class id="Production Automatisée de Documents_TD-2"/g' \
-e 's/class id="7040"/class id="Production Automatisée de Documents_TP-1"/g' \
-e 's/class id="7050"/class id="Production Automatisée de Documents_TP-2"/g' \
-e 's/class id="8000"/class id="Qt Avancé_CM-1"/g' \
-e 's/class id="8010"/class id="Qt Avancé_CM-2"/g' \
-e 's/class id="8020"/class id="Qt Avancé_TP-1"/g' \
-e 's/class id="8030"/class id="Qt Avancé_TP-2"/g' \
-e 's/class id="9000"/class id="Systèmes Intelligents_CM-1"/g' \
-e 's/class id="9010"/class id="Systèmes Intelligents_CM-2"/g' \
-e 's/class id="9020"/class id="Systèmes Intelligents_TD-1"/g' \
-e 's/class id="9030"/class id="Systèmes Intelligents_TD-2"/g' \
-e 's/class id="9040"/class id="Systèmes Intelligents_TP-1"/g' \
-e 's/class id="9050"/class id="Systèmes Intelligents_TP-2"/g' \
-e 's/" name="[^ ]*"/"/g' \
-e 's/distribution name="[^ ]*"/distribution/g' \
-e 's/" code="[^ ]*"/"/g';

## renommage des id d'étudiants (bitsets des choix d'options en symboles)
find ./jeux-avec-id -type f -name "*.xml" -print0 | xargs -0 \
sed -i -E -e 's/\-0011/\-qt\-si/g' \
-e 's/\-0101/\-pa\-si/g' \
-e 's/\-1001/\-is\-si/g' \
-e 's/\-0110/\-pa\-qt/g' \
-e 's/\-1010/\-is\-qt/g' \
-e 's/\-1100/\-is\-pa/g';

# 2.
## modification des valeurs d'attributs de créneaux ('start', etc) pour visualisation par l'interface web
find ./jeux-avec-id -type f -name "*.xml" -print0 | xargs -0 \
sed -i -E -e "s/slotsPerDay=\"5\"/slotsPerDay=\"288\"/" -e "s/length=\"1\"/length=\"16\"/" -e "s/start=\"1\"/start=\"97\"/" -e "s/start=\"2\"/start=\"115\"/" -e "s/start=\"3\"/start=\"133\"/" -e "s/start=\"4\"/start=\"169\"/" -e "s/start=\"5\"/start=\"187\"/";

## renommage et suppression de la partie problème dans le fichier solution
tr -d '[\r\n]' < ./jeux-avec-id/edt-solution-1819-l3info-s2.xml > solution.xml;
sed -i -E "s/^<\?xml version=\"1\.0\" encoding=\"UTF\-8\"\?>.*<solution\(.*\)<\/problem>$/<\?xml version=\"1\.0\" encoding=\"UTF\-8\"\?><solution\1/g" solution.xml;

# 3.
## recopie dans le répertoire parent
cp ./jeux-avec-id/edt-problem-1819-l3info-s2.xml problem.xml;
