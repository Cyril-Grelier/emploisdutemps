#/bin/sh
# transforme les minutes en rang de crénéeaux : rang 1 <=> 8h <=> 97 minutes pour interface web !

xvd=5;
xd=288;
xvL=1;
xL=16;
xv1=1;
xs1=97;
xv2=2;
xs2=115;
xv3=3;
xs3=133;
xv4=4;
xs4=169;
xv5=5;
xs5=187;

find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/slotsPerDay=\"$xd\"/slotsPerDay=\"$xvd\"/"`;
find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/length=\"$xd\"/length=\"$xvL\"/"`;
find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs1\"/start=\"$xv1\"/"`;
find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs2\"/start=\"$xv2\"/"`;
find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs3\"/start=\"$xv3\"/"`;
find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs4\"/start=\"$xv4\"/"`;
find . -type f -name "*.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs5\"/start=\"$xv5\"/"`;
