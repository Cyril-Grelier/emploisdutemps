import pickle
from statistics import mean

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()

output_directory = "output_tests_l3_pypy/"


def load_scores(file):
    with open(file, "rb") as f:
        return pickle.load(f)


with open(output_directory + "execution") as f:
    content = f.readlines()

ls_rand_recuit = {"scores": [], "times": [], "titre": "LS RAND RC"}
ls_tabou_recuit = {"scores": [], "times": [], "titre": "LS TABOU RC"}
ls_rand_n = {"scores": [], "times": [], "titre": "LS RAND"}
ls_tabou_n = {"scores": [], "times": [], "titre": "LS TABOU"}
lns_light = {"scores": [], "times": [], "titre": "LNS SS"}
lns_free = {"scores": [], "times": [], "titre": "LNS FREE"}

for line in content:
    d, t, met, rs, light, rand_init, recuit, exec_time, file = line.split()
    if "/" in file:
        file = file.split("/")[-1][:-4:]
    execution_time = float(exec_time)
    # print(met, rs, light, rand_init, recuit, execution_time, file)
    sf = "_".join([met, rs, light, rand_init, recuit, file]) + ".pck"
    scores = load_scores(output_directory + sf)
    liste = None
    if met == "ls":
        if rand_init == "True":
            if recuit == "True":
                liste = ls_rand_recuit
            else:
                liste = ls_rand_n
        else:
            if recuit == "True":
                liste = ls_tabou_recuit
            else:
                liste = ls_tabou_n
    elif met == "lns":
        if light == "True":
            liste = lns_light
        else:
            liste = lns_free
    liste["scores"].append(scores)
    liste["times"].append(execution_time)

lignes = [
    "méthode",
    "temps min",
    "temps moyen",
    "temps max",
    "score strict min",
    "score strict moyen",
    "score strict max",
    "score souple min",
    "score souple moyen",
    "score souple max",
]

for ligne in lignes:
    print(ligne, end=", ")
    for liste in [
        ls_rand_n,
        ls_tabou_n,
        ls_rand_recuit,
        ls_tabou_recuit,
        lns_light,
        lns_free,
    ]:
        if ligne == "méthode":
            print(liste["titre"], end=", ")
        elif ligne == "temps min":
            print(f'{min(liste["times"]):0.2f}', end=", ")
        elif ligne == "temps moyen":
            print(f'{mean(liste["times"]):0.2f}', end=", ")
        elif ligne == "temps max":
            print(f'{max(liste["times"]):0.2f}', end=", ")
        elif ligne == "score strict min":
            print(
                f'{min([score[-1].hard for score in liste["scores"]]):0.2f}', end=", "
            )
        elif ligne == "score strict moyen":
            print(
                f'{mean([score[-1].hard for score in liste["scores"]]):0.2f}', end=", "
            )
        elif ligne == "score strict max":
            print(
                f'{max([score[-1].hard for score in liste["scores"]]):0.2f}', end=", "
            )
        elif ligne == "score souple min":
            print(
                f'{min([score[-1].soft for score in liste["scores"]]):0.2f}', end=", "
            )
        elif ligne == "score souple moyen":
            print(
                f'{mean([score[-1].soft for score in liste["scores"]]):0.2f}', end=", "
            )
        elif ligne == "score souple max":
            print(
                f'{max([score[-1].soft for score in liste["scores"]]):0.2f}', end=", "
            )
    print()

for liste in [
    ls_rand_n,
    ls_tabou_n,
    ls_rand_recuit,
    ls_tabou_recuit,
    lns_light,
    lns_free,
]:

    scores_to_mean_hard = []
    scores_to_mean_soft = []

    for s in liste["scores"]:
        scores_to_mean_hard.append([sa.hard for sa in s if sa is not None])
        scores_to_mean_soft.append([sa.soft for sa in s if sa is not None])

    len_max = max([len(x) for x in scores_to_mean_hard])
    for x in scores_to_mean_hard:
        len_current = len(x)
        for _ in range(len_current, len_max):
            x.append(x[-1])

    for x in scores_to_mean_soft:
        len_current = len(x)
        for _ in range(len_current, len_max):
            x.append(x[-1])
    arrays = [np.array(x) for x in scores_to_mean_hard]
    liste["mean_hard"] = [np.mean(k) for k in zip(*arrays)]
    print("len de la moyenne : ", len(liste["mean_hard"]))
    arrays = [np.array(x) for x in scores_to_mean_soft]
    liste["mean_soft"] = [np.mean(k) for k in zip(*arrays)]

# for liste in [ls_rand_n, ls_tabou_n, ls_rand_recuit, ls_tabou_recuit,
#               lns_light, lns_free]:
#     fig, ax = plt.subplots()
#
#     for s in liste["scores"]:
#         if None in s:
#             plt.axvline(s.index(None), linewidth=1, color='b')
#             s = s[0:s.index(None)] + s[s.index(None) + 1::]
#         ax.plot([sa.hard for sa in s], label='Hard', color='r')
#         ax.plot([sa.soft for sa in s], label='Soft', color='#FFA500')
#     # ax.legend()
#     ax.set_xlabel('tours')
#     ax.set_ylabel('nb contraintes')
#     handles, labels = plt.gca().get_legend_handles_labels()
#     newLabels, newHandles = [], []
#     for handle, label in zip(handles, labels):
#         if label not in newLabels:
#             newLabels.append(label)
#             newHandles.append(handle)
#     plt.title(liste['titre'])
#     plt.xlim(0)
#     plt.ylim(0)
#     plt.legend(newHandles, newLabels, loc='upper right')
#     plt.show()

fig, (ax0, ax1) = plt.subplots(
    figsize=(8, 6), nrows=2, gridspec_kw={"height_ratios": [1, 1.5]}
)

for liste in [
    ls_tabou_n,
    ls_tabou_recuit,
    ls_rand_n,
    ls_rand_recuit,
    lns_light,
    lns_free,
]:
    ax0.plot(liste["mean_hard"], label=liste["titre"])
    ax1.plot(liste["mean_soft"], label=liste["titre"])

# fig.legend()
ax0.set_xlim(0)
ax0.set_ylim(0, 10)
ax1.set_xlim(0)
ax1.set_ylim(0, 1100)
ax0.set_ylabel("contraintes dures")
ax1.set_ylabel("contraintes souples")
ax0.set_xlabel("itérations")
ax1.set_xlabel("itérations")

ax1.set_title("Contraintes souples")
ax0.set_title("Contraintes dures")
# fig.tight_layout()

handles, labels = plt.gca().get_legend_handles_labels()

newLabels, newHandles = [], []
for handle, label in zip(handles, labels):
    if label not in newLabels:
        newLabels.append(label)
        newHandles.append(handle)

plt.legend(newHandles, newLabels)
# , bbox_to_anchor=(1.05, 1), loc='upper left',borderaxespad=0.)

plt.show()

# print(f'{min(liste["times"])=:0.2f}')
# print(f'{mean(liste["times"])=:0.2f}')
# print(f'{max(liste["times"])=:0.2f}')
# print('-' * 10)
# print(f'{min([score[-1].hard for score in liste["scores"]])=:0.2f}')
# print(f'{min([score[-1].soft for score in liste["scores"]])=:0.2f}')
# print(f'{mean([score[-1].hard for score in liste["scores"]])=:0.2f}')
# print(f'{mean([score[-1].soft for score in liste["scores"]])=:0.2f}')
# print(f'{max([score[-1].hard for score in liste["scores"]])=:0.2f}')
# print(f'{max([score[-1].soft for score in liste["scores"]])=:0.2f}')
# print('=' * 20)
